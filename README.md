# GCP Bucket Creator
This project allows the user to create buckets within their selected   
Google Cloud Platform Project.

This project utilizes a simple HTML5/CSS front end to trigger a   
Cloud Function written in Python 3.7

### Important:
- You must enable the Cloud Function API within your GCP Project.  
- Add ***google-cloud-storage*** to the Cloud Function requirements.txt  
- make sure to update your Cloud Function HTTP Trigger address,  
as well as the addresses for your **index.html**, **bucketexists.html**,   
and **success.html** pages, as these are specific to your GCP project.
	- There are commented areas within each HTML doc that explain  
	where you need to make these specific edits.